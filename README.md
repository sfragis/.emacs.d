# How to install

1. Copy this file under `.emacs.d` or clone repository into your home
2. If modules are not installed on their own, run `M-x package-install`
   for each module at the end of this file or simply
   `M-x package-install-selected-packages`
3. Initialize fonts: `M-x all-the-icons-install-fonts`

Restart Emacs and you should be ready to go.

## Keeping packages up-to date

As explained in the MELPA documentation, updating all MELPA packages in one go is as easy
as typing `M-x package-list-packages RET` and then:
- `r`: refreshes the package list
- `U`: marks Upgradable packages
- `x`: executes the installs and deletions

Also, to remove packages no longer needed, just type `M-x package-autoremove`. Beware, however,
that some packages may actually be required and cannot be removed (such as `use-package`).

# References

Useful documentation worth reading that inspired this configuration:

* https://github.com/magnars/.emacs.d
* http://whattheemacsd.com
* http://emacsrocks.com
* https://www.emacswiki.org/emacs/EmacsNewbieKeyReference#toc2
* https://github.com/emacs-tw/awesome-emacs

This is a good base to start with:

* https://github.com/bbatsov/crux/blob/master/crux.el


# Keybindings

Here are most of the keybindings I use (and struggle to remember...).

## Main

Keybinding                      | Description
--------------------------------|--------------------------------
`F1`                            | Open terminal buffer
`F2`                            | New unknown buffer
`C-F2`                          | Reload current buffer from filesystem
`S-F3`                          |
`M-F3`                          |
`F3`                            | Repeat last command
`C-F3`                          | Repeat last recorded macro
`S-F3`                          |
`M-F3`                          |
`F4`                            | Opens Magit
`C-F4`                          |
`S-F4`                          |
`M-F4`                          |
`F5`                            | Toggle line wrapping
`C-F5`                          |
`S-F5`                          |
`M-F5`                          |
`F6`                            | Open files' directory in file explorer (with current file or marked files selected), also works in Dired
`C-F6`                          | Open file(s) externally (using OS associated application)
`M-F6`                          | Open file's directory in terminal
`F7`                            | Toggle other window position (bottom or right) when two windows are shown
`F8`                            | Browse URL under file or selected
`F9`                            | Open the speedbar
`S-F10`                         | Show contextual menu
`S-F11`                         | Toggle line numbers
`C-F11`                         | Toggle line highlightening
`M-F11`                         | Toggle menu bar
`S-F12`                         | Toggle presentation mode; use `C-x C-+` or `C-x C--` to enlarge or shrink font, use `C-x C-0` to reset font size
`C-F12`                         | Toggle full screen
`M-F12`                         | Toggle distraction (Olivetti) free mode
`C-<left-click>`                | Start rectangular selection by ctrl-clicking with left button
`C-S-<left-click>`              | Add one more cursor to clicked location
`S-<scroll wheel down/up>`      | Horizontally scroll the buffer right or left by 1 char
`C-<scroll wheel down/up>`      | Horizontally scroll the buffer right or left by 5 char
`C-<pgup,pgdown>`               | Jump to tab on the left or right
`C-S-<pgup,pgdown>`             | Move tab left or right
`C-q TAB`                       | Insert a real TAB
`C-q C-m`                       | Insert a real "carriage return"
`C-q C-j`                       | Insert a real "line feed"
`S-M-u`                         | Toggle case for selected text
`s-d`                           | Duplicate line or selected text
`s-i`                           | Toggle overwrite mode
`s-<backspace>`, `C-S-k`        | Kill whole line
`C-k`                           | Kill to end of line first
`C-w`                           | Kill to beggining of line
`C-q <CHAR>`                    | Insert a raw `<CHAR>`, useful to insert a tab
`C-x TAB <left,right>`          | (Un)indent selection or line; press arrows multiple times to (un)indent more than once
`C-h b`                         | Show available bindings
`C-x C-e`                       | Eval LISP expression
`C-x C-S-e`                     | Eval LISP expression and replaces it with its value, very handy for calculations
`s-[`                           | Move to previous position
`C-x C-a`                       | Select the entire buffer
`C-x b`, `C-x C-b`              | Swtich minibuffer autocompletion
`C-x C-g`                       | Starts Magit
`C-x C-d`                       | Open dired
`C-x C-f`                       | Open file with IDO (can switch to non-IDO mode by pressing `C-x C-f` again)
`C-x C-r`                       | Open recent files
`C-x C-q`                       | Toggle read-only mode
`C-x u`                         | Show the undo tree
`C-x w`                         | Toggle line wrapping
`s-z`, `C-z`, `C-S-z`, `C-/`    | Undo
`C-S-z`, `s-S-Z`, `C-_`         | Redo
`C-;`                           | Toggle line comment
`M-;`                           | Comment selection or add comment at the end of line
`C-=`, `C-- C-=`                | Expand, unexpand selection
`M-:`                           | Evaluate expression
`M-x`                           | Execute an interactive command
`M-/`                           | Expand text (autocomplete word)
`M-j`                           | Join current line with following one
`M-q`                           | Break long lines in shorter ones
`M-S-<up,down>`                 | Move line or selected lines up or down
`M-RETURN`                      | Insert a line above
`C-RETURN`, `S-RETURN`          | Insert a line below
`s-y`                           | Inserts the `λ` symbol that represents an interactive lamba function


### Searching and moving inside the buffer

Keybinding                      | Description
--------------------------------|--------------------------------
`C-r`                           | Search backward; can search selected text, press `M-c` to toggle case.
`C-s`                           | Search forward; can search selected text, press `M-c` to toggle case.
`M-e`                           | Move the cursor to the minibuffer to edit the searching text
`M-c`                           | Toggles case during search
`M-p`, `M-n`                    | Move between previous (p) and next (n) queries
`C-M-r`                         | Search regexp backward
`C-M-s`                         | Search regexp forward
`M-g M-g`, `M-g g`, `s-l`       | Go to line
`M-g TAB`                       | Go to column
`s-<up>`, `M-<`                 | Move to beginning of buffer
`s-<down>`,`M->`                | Move to end of buffer

### Open in other window or frame

Two prefixes are very useful to control what is opened in a new window (panel) or frame (window).

Prefix                          | Description
--------------------------------|--------------------------------
`C-x 4`                         | Open in a new side window
`C-x 5`                         | Open in a new frame

Then use one of these keywords:

Keybinding                      | Description
--------------------------------|--------------------------------
`b`                             | Open buffer
`d`                             | Open dired
`f`                             | Open file
`r`                             | Open file read-only

### Git gutter

Keybinding   | Description
-------------|-----------
`C-,`, `C-.` | Move to previous, next Git changes (if Git Gutter is enabled)
`C-±`        | Popups this Git hunk


### Navigating between buffers, frames and windows

Keybinding                      | Description
--------------------------------|--------------------------------
`C-x <left>`, `s-{`             | Move to previous buffer
`C-x <right>`, `s-}`            | Move to next buffer
`M-s-<arrow>`                   | Move between windows with arrows
`s-w`, `C-F4`                   | Kill current buffer
`s->`                           | Next window
`s-<`                           | Previous window
`s-+`                           | Increase text size
`s--`                           | Decrease text size
`s-0`                           | Reset text size
`S-<ESC>`, `C-x 1`              | Closes other window (not frame!) just like IntelliJ does


### Multi cursors

Keybinding | Description
-----------|-----------
`<ENTER>`  | Removes all multiple cursors
`C-j`      | Insert a newline
`C->`      | Mark next text like the selected one (`C-g` twice to exit), or add a cursor below if no text is selected
`C-<`      | Mark previous text like the selected one (`C-g` twice to exit), or add a cursor above if no text is selected
`C-~`      | Mark **all** text like the selected one
`C-M->`    | Unmark next text like the selected one
`C-M-<`    | Unmark previous text like the selected one


### Smartparens

Keybinding | Description
-----------|-----------
`C-M-a`    | Beginning of smartparens expression
`C-M-e`    | End of smartparens expression
`C-M-n`    | Next beginning of smartparens expression
`C-M-b`    | Previous beginning of smartparens expression
`C-M-f`    | Next end of smartparens expression
`C-M-p`    | Previous end of smartparens expression


### Rectangular selection

Toggles rectangular selection with `C-x <SPACE>`:

Keybinding | Description
-----------|-----------
`C-x C-x`  | Exchanges point and mark: moves the cursor to each edge to allow you to expand and shrink the selection in any direction
`C-t`      | Allows insertion, when done press `<RET>`


### Macros

Keybinding  | Description
------------|------------
`C-x (`     | Start recording a keyboard macro
`C-x )`     | Stops recording a keyboard macro
`C-x C-k n` | Gives a name to the macro


### Registers

Use any of `s-r` or `C-x r` as the "prefix" to begin manipulating registers. Then press one of the following
shortcuts (remember that register's names are single character):

Shortcut | Description
---------|---------
`s`      | Saves the selected text into a register
`n`      | Saves a number into a register (use `C-u <number>` before to save `<number>`)
`i`      | Gets the value of a register and inserts it into the text
`+`      | Increments a register number
`<SPACE>`| Saves a position
`w`      | Saves a window configuration
`f`      | Saves a frame configuration (better use this when desktop file is saved)
`j`      | Jumps back to a position or window configuration

#### More help

-  A tour of registries: https://www.youtube.com/watch?v=u1YoF4ycLTY


### Special characters

Right ALT can be used to compose special characters as usual with MacOS.

`CTRL+x 8 CTRL+h` display a help window with all of the key sequences for accented characters. Some are:

Keybinding      | Description
----------------|----------------
`C-x 8 <enter>` | Inserts any character, the minibuffer will let you search through them
`C-x 8 <`       | «
`C-x 8 >`       | »
`C-x 8 =`       | ¯
`C-x 8 C`       | ©
`C-x 8 P`       | ¶
`C-x 8 m`       | µ


## Distraction

Keybinding | Description
-----------|-----------
`C-c {`    | Shrink body in distraction free mode (`{` can be repeated multiple times)
`C-c }`    | Enlarge body in distraction free mode (`}` can be repeated multiple times)


## Projectile

`C-c p` is the prefix keymap for Projectile.

Keybinding | Description
-----------|-----------
`C-c p p`  | Open project
`C-c p f`  | Open file in project


## Magit

First, run `C-c C-g` to open the Magit status buffer. Then use the shortcut

Shortcut|Description
--------|--------
`<tab>` | Expands, collapses sections (like `>modified README.md`)
`Fu`    | Pulls from remote upstream
`Pu`    | Pushes to remote upstream
`s`     | Stages a file
`S`     | Stages all files
`u`     | Unstages a single file
`U`     | Unstages all files
`cc`    | Creates a new commit for the staged changes
`ca`    | Amends the last commit
`ce`    | Amends the last commit without editing the message
`C-c`   | While editing a commit message, saves it and commits
`h`     | To open the help
`C-g`   | To quit the help
`q`     | To qui the Magit buffer


## Dired

Type `C-x d` to choose where to open Dired or `C-x C-d` to open at the current file directory.
Then you can move through the files and directories using arrows and `<ENTER>`.


Shortcut      | Description
--------------|--------------
`^` (SHIFT-6) | Navigate to upper directory
`g`           | Refresh buffer
`<ENTER>`     | Open a file in edit mode or enters a directory
`o`           | Open a file in a side window
`m`           | Mark a file
`u`           | Unmark a marked file
`U`           | Unmark all marked files
`c`           | Copy marked files
`r`           | Rename a marked file
`v`           | Open a file in read-only mode
`M-s f C-s`   | Search for file incrementally
`C-o`         | Open file in new window
`Z`           | Compress o uncompress file
`t`			  | Invert selection (marked files)
`+`           | Create a new directory

You can use `%` as a prefix key to the above keys in order to use regular expressions.

Also '*' works like a prefix for predefined selection patterns (like '/' directories and '*' executables).

For more shortcuts please refer to: https://www.gnu.org/software/emacs/manual/html_node/emacs/Dired.html


## Org

Use `C-c` as a prefix and then:

Shortcut                | Description
------------------------|------------------------
`M-<LEFT>`, `M-<RIGHT>` | Move columns of a table right and left
`S-<LEFT>`, `S-<RIGHT>` | Cycle through TODO states
`<RET>`                 | Insert a new heading
`S-M-<RET>`             | Insert a new TODO entry after the current one
`.`                     | Insert a new date from calendar
`C-c`                   | Toggle the state of a checkbox (from `[ ]` to `[X]`)
`C-x C-i`               | Starts the clock for the current item
`C-x C-o`               | Stops the clock for the current item
`C-x C-q`               | Cancels the clock for the current item
`C-c / T`               | Shows only items with specified todo-status (use `|` to separate multiple ones)
`C-c C-x a`             | Toggles `:ARCHIVE:` tag to old items so that they are always collpased (unless un-tagged)

See here for even more information:
- Table howto: https://orgmode.org/manual/Built_002din-Table-Editor.html
- Org as spreadsheet: https://orgmode.org/worg/org-tutorials/org-spreadsheet-intro.html

### ORG Roam

Use `C-c n f` to open a (new) Roam note. Inside a note, use these keybindings:

Shortcut  | Description
----------|----------
`<TAB>`   | Toggle section expansion
`C-c C-c` | Save the new note (not just the file)
`C-c C-o` | Open the node at point


## Rest client keybindings

Please watch https://www.gnu.org/savannah-checkouts/gnu/emacs/videos/emacs-rocks-15.ogg first

Remember you can use headers, body and variables to send your requests. Requests are separated by comments
(that start with `#`).

Use `C-c C-c` to send the request. That's it!


# Todo

* Look here for sources of
    * good config file: https://github.com/xenodium/dotsies/tree/main/emacs
    * completion system: https://config.daviwil.com/emacs#completion-system
    * dired: https://config.daviwil.com/emacs#file-browsing
    * org: https://config.daviwil.com/emacs#org-mode
    * https://github.com/jakebox/jake-emacs/blob/main/jake-emacs/init.el
* A solution for un-indent: https://ignaciopp.wordpress.com/2009/06/17/emacs-indentunindent-region-as-a-block-using-the-tab-key/
* Give a look at: https://protesilaos.com/emacs/dotemacs#h:b24ce3fc-a12c-4d21-93d7-c1e7bd36a65d
* Give a try to this theme:
    * https://protesilaos.com/emacs/modus-themes#h:3f3c3728-1b34-437d-9d0c-b110f5b161a9
    * https://www.youtube.com/watch?v=JJPokfFxyFo
