(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("8721f7ee8cd0c2e56d23f757b44c39c249a58c60d33194fe546659dabc69eebd" "7dc296b80df1b29bfc4062d1a66ee91efb462d6a7a934955e94e786394d80b71" "3199be8536de4a8300eaf9ce6d864a35aa802088c0925e944e2b74a574c68fd0" "f5b6be56c9de9fd8bdd42e0c05fecb002dedb8f48a5f00e769370e4517dde0e8" "285d1bf306091644fb49993341e0ad8bafe57130d9981b680c1dbd974475c5c7" "57a29645c35ae5ce1660d5987d3da5869b048477a7801ce7ab57bfb25ce12d3e" "9fe9462ddb8d826c7b2c9991ce79d5e90afbeed8846294d4bc69a2893149ef12" "4c56af497ddf0e30f65a7232a8ee21b3d62a8c332c6b268c81e9ea99b11da0d3" "fee7287586b17efbfda432f05539b58e86e059e78006ce9237b8732fde991b4c" "05626f77b0c8c197c7e4a31d9783c4ec6e351d9624aa28bc15e7f6d6a6ebd926" "2dc03dfb67fbcb7d9c487522c29b7582da20766c9998aaad5e5b63b5c27eec3f" "d36399e2e1d256464562a3b9e543267ebf6dc7b38e2766f7f26fe021db925b87" "0b7861ad7f8578f55a69971da5c7a7fa1f86e47c6428ee026208cfea9e7184e8" "fefb007c42fb53c7d6c2c640e697d82316fb9a45fd2b512ee8f5bb53b3b866b6" "f0eb51d80f73b247eb03ab216f94e9f86177863fb7e48b44aacaddbfe3357cf1" "db5b906ccc66db25ccd23fc531a213a1afb500d717125d526d8ff67df768f2fc" "98fada4d13bcf1ff3a50fceb3ab1fea8619564bb01a8f744e5d22e8210bfff7b" "a556e4e6fc62469cd28a57c3b5386807d676a33176659f849fc53fa8763f5955" "24714e2cb4a9d6ec1335de295966906474fdb668429549416ed8636196cb1441" default))
 '(package-selected-packages
   '(marginalia emacsql-sqlite-builtin nyan-mode diminish dracula-theme org-roam dired-git-info diredfl corfu vertico restclient dwim-shell-command company-coq proof-general benchmark-init all-the-icons-dired pdf-tools goto-last-change nov utop merlin tuareg slime rust-mode yasnippet exec-path-from-shell scala-mode plantuml-mode olivetti php-mode presentation yaml-mode markdown-mode+ smartparens crux which-key multiple-cursors expand-region xkcd doom-modeline undo-tree projectile all-the-icons dashboard git-gutter typescript-mode magit smex move-text dockerfile-mode))
 '(tab-width 4))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(cursor ((t (:background "#990A1B")))))
