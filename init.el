;; The default is 800 kilobytes.  Measured in bytes.
(setq gc-cons-threshold (* 50 1024 1024)
      gc-cons-percentage 0.6)

(add-hook 'emacs-startup-hook
          (lambda ()
            (message "*** Emacs loaded in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;; restore default gc settings after startup
(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold (* 16 1024 1024)
                  gc-cons-percentage 0.1)
            ))

;; loads package manager
(load (locate-user-emacs-file "fs/package.el"))

;; define this early
(defmacro λ (&rest body)
  "Shorthand for interactive lambdas."
  `(lambda () (interactive) ,@body))

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Benchmarking
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Remember to disable collection of benchmark data after init is done.
;(require 'benchmark-init)
;(add-hook 'after-init-hook 'benchmark-init/deactivate)

;; Local libraries (not packaged)
(add-to-list 'load-path "~/.emacs.d/libs")

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; File for customized variables
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(setq custom-file (locate-user-emacs-file "custom-vars.el"))
(load custom-file 'noerror 'nomessage)

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Config files
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(load (locate-user-emacs-file "fs/core.el"))
(load (locate-user-emacs-file "fs/functions.el"))
(load (locate-user-emacs-file "fs/macos.el"))
(load (locate-user-emacs-file "fs/backup.el"))
(load (locate-user-emacs-file "fs/keybindings.el"))
(load (locate-user-emacs-file "fs/editing.el"))
(load (locate-user-emacs-file "fs/enhancements.el"))
(load (locate-user-emacs-file "fs/dired.el"))
(load (locate-user-emacs-file "fs/git.el"))
(load (locate-user-emacs-file "fs/completion.el"))
(load (locate-user-emacs-file "fs/org.el"))
(load (locate-user-emacs-file "fs/ocaml.el"))
(load (locate-user-emacs-file "fs/languages.el"))
(load (locate-user-emacs-file "fs/misc.el"))

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Theme
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(load-theme 'dracula)
(put 'upcase-region 'disabled nil)

(provide 'init)
