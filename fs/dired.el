;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Dired
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

(use-package dired
  :ensure nil
  :init
  (setq dired-kill-when-opening-new-dired-buffer t)
  :commands
  (dired dired-jump)
  :bind
  (("C-x C-d" . dired-jump))
  :config
  (when (string= system-type "darwin")
  (setq dired-use-ls-dired t
        insert-directory-program "/usr/local/bin/gls"
        dired-listing-switches "-hl --group-directories-first"))
  (use-package diredfl
    :config
    (diredfl-global-mode 1))
  (use-package dired-git-info
    :bind (:map dired-mode-map
                (")" . dired-git-info-mode)))
  :hook
  (
   (dired-mode . all-the-icons-dired-mode)
   (dired-mode . dired-hide-details-mode)
   )
  )

;; (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;; (setq dired-listing-switches "-lh")
