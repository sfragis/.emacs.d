;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Keybindings
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

;; main
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "s-y") (λ (insert "\u03bb"))) ; win+y inserts λ
(global-set-key (kbd "C-x C-b") 'switch-to-buffer)
(global-set-key (kbd "s-b") 'switch-to-buffer)
(global-set-key (kbd "C-x w") 'toggle-truncate-lines)
(global-set-key (kbd "C-x C-r") 'recentf-open-files)
(global-set-key (kbd "C-<down-mouse-1>") #'fs/mouse-start-rectangle)
(global-set-key (kbd "S-<mouse-4>") (λ (if truncate-lines (scroll-right 1))))
(global-set-key (kbd "C-<mouse-4>") (λ (if truncate-lines (scroll-right 5))))
(global-set-key (kbd "S-<mouse-5>") (λ (if truncate-lines (scroll-left 1))))
(global-set-key (kbd "C-<mouse-5>") (λ (if truncate-lines (scroll-left 5))))
(global-set-key [C-mouse-4] 'text-scale-increase)
(global-set-key [C-mouse-5] 'text-scale-decrease)
(global-set-key (kbd "S-<escape>") 'delete-other-windows)

;; F-keys
(global-set-key (kbd "<f2>") 'fs/new-empty-buffer)
(global-set-key (kbd "C-<f2>") 'revert-buffer)
(global-set-key (kbd "<f3>") 'repeat)
(global-set-key (kbd "C-<f3>") 'kmacro-end-and-call-macro)
(global-set-key (kbd "<f5>") 'toggle-truncate-lines)
(global-set-key (kbd "M-<f6>") 'fs/open-in-terminal)
(global-set-key (kbd "<f7>") 'fs/toggle-window-split)
(global-set-key (kbd "<f8>") 'browse-url)
(global-set-key (kbd "<f9>") 'speedbar)
(global-set-key (kbd "S-<f11>") 'display-line-numbers-mode)
(global-set-key (kbd "C-<f11>") 'hl-line-mode)
(global-set-key (kbd "M-<f11>") 'menu-bar-mode)
(global-set-key (kbd "C-<f12>") 'toggle-frame-fullscreen)

;; Editing shortcuts
(global-set-key (kbd "<backtab>") 'fs/unindent)
(global-set-key (kbd "M-j") (λ (join-line -1)))
(global-set-key (kbd "M-U") 'fs/toggle-case)
(global-set-key (kbd "C-S-k") 'kill-whole-line)
(global-set-key (kbd "s-<backspace>") 'kill-whole-line)
(global-set-key (kbd "C-x C-a") 'mark-whole-buffer) ; same as C-x h
(global-set-key (kbd "C-;") 'fs/comment-lines-toggle)
(global-set-key (kbd "s-d") 'fs/duplicate-line-or-region)
(global-set-key (kbd "C-x C-S-e") 'crux-eval-and-replace)
(global-set-key [M-S-up] 'move-text-up)
(global-set-key [M-S-down] 'move-text-down)
(global-set-key (kbd "M-<return>") (λ (beginning-of-line)(open-line 1)))
(global-set-key (kbd "C-<return>") (λ (end-of-line)(newline)))
(global-set-key (kbd "S-<return>") (λ (end-of-line)(newline)))
(global-set-key (kbd "s-r") (lookup-key global-map (kbd "C-x r")))
(global-set-key (kbd "s-i") 'overwrite-mode)

;; Moving around
(global-set-key (kbd "<home>") 'move-beginning-of-line)
(global-set-key (kbd "<end>") 'end-of-line)
(global-set-key (kbd "s-<up>") 'beginning-of-buffer)
(global-set-key (kbd "s-<down>") 'end-of-buffer)
(global-set-key (kbd "s-w") 'kill-current-buffer)
(global-set-key (kbd "s-§") 'other-frame)
(global-set-key (kbd "s->") 'next-window-any-frame)
(global-set-key (kbd "s-<") 'previous-window-any-frame)
(global-set-key (kbd "s-[") 'pop-global-mark)
(global-set-key (kbd "s-}") 'next-buffer)
(global-set-key (kbd "s-{") 'previous-buffer)
