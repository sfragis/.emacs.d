;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Ocaml
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(use-package tuareg
  :config
  (add-hook 'tuareg-mode-hook #'electric-pair-local-mode)
  ;; (add-hook 'tuareg-mode-hook 'tuareg-imenu-set-imenu)
  (setq auto-mode-alist
        (append '(("\\.ml[ily]?$" . tuareg-mode)
                  ("\\.topml$" . tuareg-mode))
                auto-mode-alist)))

(use-package merlin
  :config
  (add-hook 'tuareg-mode-hook 'merlin-mode)
  (add-hook 'merlin-mode-hook #'company-mode)
  (setq merlin-error-after-save nil))

(use-package company-coq
  :config
  (require 'proof-site "~/.emacs.d/lisp/PG/generic/proof-site")
  ;; Load company-coq when opening Coq files
  (add-hook 'coq-mode-hook #'company-coq-mode)
  )

(use-package utop
  :config
  (autoload 'utop-minor-mode "utop" "Minor mode for utop" t)
  (add-hook 'tuareg-mode-hook 'utop-minor-mode)
  )
