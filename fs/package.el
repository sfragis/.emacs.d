;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Package Manager
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
;;(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3") ;; w/o this Emacs freezes when refreshing ELPA

(unless (bound-and-true-p package--initialized)
  (setq package-enable-at-startup nil) ; To prevent initializing twice
  (package-initialize))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
    (package-refresh-contents) ; update archives
  (package-install 'use-package)) ; grab the newest use-package
(setq use-package-always-ensure t)
(setq use-package-expand-minimally t)
(setq use-package-compute-statistics t)
(setq use-package-verbose nil)
(require 'use-package)

;; This package hides the minor modes from the modeline
(use-package diminish)
