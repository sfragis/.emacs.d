;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Tree-sitter
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; run this to install everything:
;; (mapc #'treesit-install-language-grammar (mapcar #'car treesit-language-source-alist))
;; please see: https://www.masteringemacs.org/article/how-to-get-started-tree-sitter
(setq treesit-language-source-alist
   '((bash "https://github.com/tree-sitter/tree-sitter-bash")
     (c "https://github.com/tree-sitter/tree-sitter-c")
     (cmake "https://github.com/uyha/tree-sitter-cmake")
     (css "https://github.com/tree-sitter/tree-sitter-css")
     (elisp "https://github.com/Wilfred/tree-sitter-elisp")
     (dockerfile "https://github.com/camdencheek/tree-sitter-dockerfile")
     (go "https://github.com/tree-sitter/tree-sitter-go")
     (html "https://github.com/tree-sitter/tree-sitter-html")
     (javascript "https://github.com/tree-sitter/tree-sitter-javascript")
     (java "https://github.com/tree-sitter/tree-sitter-java")
     (json "https://github.com/tree-sitter/tree-sitter-json")
     (make "https://github.com/alemuller/tree-sitter-make")
     (markdown "https://github.com/ikatyang/tree-sitter-markdown")
     (ocaml "https://github.com/tree-sitter/tree-sitter-ocaml" "master" "ocaml/src")
     (python "https://github.com/tree-sitter/tree-sitter-python")
     (php "https://github.com/tree-sitter/tree-sitter-php")
     (rust "https://github.com/tree-sitter/tree-sitter-rust")
     (scala "https://github.com/tree-sitter/tree-sitter-scala")
     (toml "https://github.com/tree-sitter/tree-sitter-toml")
     (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
     (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
     (yaml "https://github.com/ikatyang/tree-sitter-yaml")))

(setq major-mode-remap-alist
 '((bash-mode . bash-ts-mode)
   (css-mode . css-ts-mode)
   (js2-mode . js-ts-mode)
   (json-mode . json-ts-mode)
   (rust-mode . rust-ts-mode)
   (typescript-mode . typescript-ts-mode)
   (python-mode . python-ts-mode)
   (yaml-mode . yaml-ts-mode)
   ))

(add-to-list 'auto-mode-alist '("Dockerfile\\(?:\\.*\\)?" . dockerfile-ts-mode))

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Markdown
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; PlantUML
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Use C-c C-c to preview a PlantUML buffer
;; Run plantuml-mode to activate on a buffer
(use-package plantuml-mode
  :config
  (setq plantuml-jar-path "/Users/fast/Applications/plantuml.jar")
  (setq plantuml-default-exec-mode 'jar)
  (setq plantuml-indent-level 4)
  (setq plantuml-output-type "png")
  ;; (add-to-list 'iimage-mode-image-regex-alist '("@startuml\s+\\(.+\\)" . 1))
  (add-to-list 'auto-mode-alist '("\\.puml\\'" . plantuml-mode))
  )

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Lisp - SLIME mode
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;LOCAL=/Users/fast/Projects/I-Tel/bin
;export PATH=$PATH:$LOCAL/bin
;export SBCL_HOME=$LOCAL/lib/sbcl
(setq inferior-lisp-program "sbcl")
(setq slime-lisp-implementations
      '((sbcl ("/Users/fast/Projects/I-Tel/bin/bin/sbcl")
              :env ("SBCL_HOME=/Users/fast/Projects/I-Tel/bin/lib/sbcl")
              :coding-system utf-8-unix)))

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Rust
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; (use-package cargo
;;   :defer t)
