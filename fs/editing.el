;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Highlight matching delimiters
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(use-package paren
  :ensure nil
  :config
  (setq show-paren-delay 0.1
        show-paren-highlight-openparen t
        show-paren-when-point-inside-paren t
        show-paren-when-point-in-periphery t
        blink-matching-paren t             ; Blinking parenthesis.
        )
  (show-paren-mode 1))

;; nice introduction to smartparens
;; https://ebzzry.io/en/emacs-pairs/
;; see also https://ebzzry.io/en/emacs-pairs/
(use-package smartparens
  :diminish smartparens-mode
  :config
  (setq sp-show-pair-from-inside nil)
  (require 'smartparens-config)
  :init
  (smartparens-global-mode)
  :bind
  ("C-M-a"   . sp-beginning-of-sexp)
  ("C-M-e"   . sp-end-of-sexp)
  ("C-M-f"   . sp-forward-sexp)
  ("C-M-b"   . sp-backward-sexp)
  ("C-M-n"   . sp-next-sexp)
  ("C-M-p"   . sp-previous-sexp)
  ("M-["     . sp-backward-unwrap-sexp)
  ("M-]"     . sp-unwrap-sexp)
  ("C-x C-t" . sp-transpose-hybrid-sexp)
  )

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Unique buffer names
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Recent files and history
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(use-package recentf
  :ensure nil
  :config
  (setq
   ;;recentf-auto-cleanup 'never
   ;; recentf-max-menu-items 0
   recentf-max-saved-items 200)
  (setq recentf-filename-handlers ;; Show home folder path as a ~
        (append '(abbreviate-file-name) recentf-filename-handlers))
  (recentf-mode))

;; Persists history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :ensure nil
  :init
  (setq history-length 50
        history-delete-duplicates t
        savehist-save-minibuffer-history 1
        savehist-additional-variables '(kill-ring search-ring regexp-search-ring))
  (savehist-mode))


;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Packages that simplify editing
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

;; simplifies some things like terminal, file rename and kill line
(use-package crux
  :bind
  ("C-x t" . crux-visit-term-buffer)
  ("<f1>" . crux-visit-term-buffer)
  ("C-k" . crux-smart-kill-line)
  ("<f3>" . crux-rename-file-and-buffer)
  ([remap move-beginning-of-line] . crux-move-beginning-of-line)
  )

;; simpleclip gives a more natural experience with copy/cut/past
;; not working as of emacs 29 (4th aug 2023)
;; (use-package simpleclip
;;   :init
;;   (simpleclip-mode))

;; use C-= to expand, C-- C-= to reduce
(use-package expand-region
  :bind
  ("C-=" . er/expand-region)
  )

;; See: https://github.com/magnars/multiple-cursors.el
(use-package multiple-cursors
  :bind
  ("C->" . mc/mark-next-like-this)
  ("C-<" . mc/mark-previous-like-this)
  ("C-~" . mc/mark-all-like-this)
  ("C-M->" . mc/unmark-next-like-this)
  ("C-M-<" . mc/unmark-previous-like-this)
  ("C-S-<mouse-1>" . mc/add-cursor-on-click)
  )

;; moving to last change
(use-package goto-last-change
  :bind
  ("S-s-<backspace>" . goto-last-change)
  )

;; by default emacs uses C-/ and C-? for undo and redo
;; use "C-x u" to show the undo tree
(use-package undo-tree
  :init
  (setq undo-tree-history-directory-alist (list (cons "." (concat user-emacs-directory "undos"))))
  (global-undo-tree-mode)
  :bind
  ("C-z" . undo)
  ("s-z" . undo)
  ("C-Z" . redo)
  ("s-Z" . redo)
  )
