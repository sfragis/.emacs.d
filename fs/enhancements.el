;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; All the icons
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; remember to run M-x all-the-icons-install-fonts before using for the first time
(use-package all-the-icons)

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Doom modeline
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; see https://github.com/seagle0128/doom-modeline
(use-package doom-modeline
  :config
  (setq doom-modeline-height 25)
  (setq doom-modeline-project-detection 'project)
  :init
  (doom-modeline-mode 1)
  )

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Dashboard
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(use-package dashboard
  :init
  (setq dashboard-startup-banner 2
        dashboard-set-heading-icons t
        dashboard-set-file-icons t
        dashboard-center-content t
        dashboard-banner-logo-title "[ Μ Ο Λ Ὼ Ν  Ⓥ  Λ Α Β Έ ]" ; μολὼν λαβέ
        dashboard-items '((recents  . 15)
                          (projects . 10)
                          (agenda . 10)
                          (bookmarks . 5)
                        ))
  :config
  (dashboard-setup-startup-hook)
  )

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Presentation
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Use C-x C-+ or C-x C-- to increase and decrease font size
;; Use C-x C-0 to reset font size
(use-package presentation
  :bind
  ("<f12>" . presentation-mode))

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Distraction mode
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(use-package olivetti
  :config
  (setq olivetti-body-width 0.6)
  :bind
  ("M-<f12>" . olivetti-mode))

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Which-key
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; use "M-x which-key-show-top-level" to show top level bindings
(use-package which-key
  :init
  (setq which-key-popup-type 'side-window
        which-key-side-window-location 'bottom
        which-key-sort-order 'which-key-key-order-alpha
        which-key-min-display-lines 6
        which-key-max-display-columns nil
        which-key-show-early-on-C-h t
        which-key-prefix-prefix "◉ ")
  :config
  (add-hook 'after-init-hook 'which-key-mode)
  )

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Projectile
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; see https://docs.projectile.mx/projectile/usage.html
(use-package projectile
  :config
  (projectile-mode +1)
  :bind-keymap
  ("C-c p" . projectile-command-map))

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Dwim shell commands are very handy
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(use-package dwim-shell-command
  :init
  (require 'dwim-shell-commands)
  :bind
  ("<f6>" . dwim-shell-commands-macos-reveal-in-finder)
  ("C-<f6>" . dwim-shell-commands-open-externally)
  )

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Nyan Cat
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(use-package nyan-mode
  :ensure t
  :defer 20
  :if (display-graphic-p)
  ;; :validate-custom
  ;; (nyan-bar-length 10)
  :config
  (nyan-mode +1)
  )
