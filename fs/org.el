;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Org
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; (use-package org-modern
;;   :hook (org-mode . org-modern-mode)
;;   :config
;;   (setq org-modern-star '("●" "○" "✸" "✿")
;;         org-modern-list '((42 . "◦") (43 . "•") (45 . "–"))
;;         ;; org-ellipsis " ⤵"
;;         ;; org-hide-emphasis-markers nil
;;         ;; org-pretty-entities t
;;         ;; org-modern-tag nil
;;         ;; org-modern-priority nil
;;         ;; org-modern-todo nil
;;         ))
(use-package org
  :pin gnu
  :init

  (defun fs/org-setup ()
    (org-indent-mode) ;; Keeps org items like text under headings, lists, nicely indented
    (visual-line-mode 1) ;; Nice line wrapping
    ;; (centered-cursor-mode) ;; Enable centered cursor mode
    (smartparens-mode 0) ;; Disable smartparents
    ;; (hl-prog-extra-mode) ;; Highlighting with regexps
    )

  (setq org-ellipsis " ▼ " ;;  ▿ ⤵ ▼ ⬎
        org-todo-keywords '((type
                           "TODO(t)" "WIP(w)"
                           "|" "DONE(d)" "CANCELLED(c)"))
        org-todo-keyword-faces '(
                                 ("WIP"  :inherit (org-todo) :foreground "DarkOrange1"   :weight bold)
                                 ("CANCELLED"  :inherit (org-todo) :foreground "VioletRed3"   :weight bold)
                                 )
        org-lowest-priority ?F  ;; Gives us priorities A through F
        org-default-priority ?E ;; If an item has no priority, it is considered [#E].
        org-priority-faces '((65 . "red2")
                             (66 . "Gold1")
                             (67 . "Goldenrod2")
                             (68 . "PaleTurquoise3")
                             (69 . "DarkSlateGray4")
                             (70 . "PaleTurquoise4"))
        )

  :config
  ;; Then you can edit a plantuml code block with plantuml-mode by hitting C-' while inside of the code block itself:
  ;; #+BEGIN_SRC plantuml
  ;;   <hit C-c ' here to open a plantuml-mode buffer>
  ;; #+END_SRC
  ;; When in the plantuml-mode buffer you can then hit again C-' to return to the original org-mode document.
  (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))

  :hook (org-mode . fs/org-setup)
  :diminish org-indent-mode
  :diminish visual-line-mode
  )

(use-package org-roam
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/RoamNotes")
  (org-roam-completion-everywhere t)
  (org-support-shift-select t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         :map org-mode-map
         ("C-M-i"  . completion-at-point))
  :config
  (org-roam-setup)
  )
