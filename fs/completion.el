;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Completion in minibuffer and at-point
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Makes use of these packages:
;; - vertico: for minibuffer
;; - marginalia: to have reach descriptions in minibuffer
;; - corfu: to have in-place autocompletion
;;
;; Vertico
;; https://github.com/minad/vertico
;; Example use with Prescient:
;; https://github.com/minad/vertico/wiki#using-prescientel-filtering-and-sorting
(use-package vertico
  :init
  (vertico-mode)

  (defun vertico-prescient-remember ()
  "Remember the chosen candidate with Prescient."
  (when (>= vertico--index 0)
    (prescient-remember
     (substring-no-properties
      (nth vertico--index vertico--candidates)))))

  (setq
   ;; Show more candidates
   ;;vertico-count 20
   ;; Different scroll margin
   vertico-scroll-margin 0
   ;; Grow and shrink the Vertico minibuffer
   vertico-resize t
   ;; Optionally enable cycling for `vertico-next' and `vertico-previous'.
   vertico-cycle t
   ;; Use `prescient-completion-sort' after filtering.
   vertico-sort-function #'prescient-completion-sort)

  ;; register advice
  (advice-add #'vertico-insert :after #'vertico-prescient-remember)

  ;; Configure `prescient.el' filtering to your liking.
  (setq completion-styles '(prescient basic)
        prescient-filter-method '(literal initialism prefix regexp)
        prescient-use-char-folding t
        prescient-use-case-folding 'smart
        prescient-sort-full-matches-first t ; Works well with `initialism'.
        prescient-sort-length-enable t)

  ;; Save recency and frequency rankings to disk, which let them become better
  ;; over time.
  (require 'prescient)
  (prescient-persist-mode 1)
  )

(use-package corfu
  ;; Optional customizations
  ;; :custom
  ;; (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  ;; (corfu-auto t)                 ;; Enable auto completion
  ;; (corfu-separator ?\s)          ;; Orderless field separator
  ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  ;; (corfu-preview-current nil)    ;; Disable current candidate preview
  ;; (corfu-preselect-first nil)    ;; Disable candidate preselection
  ;; (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  ;; (corfu-echo-documentation nil) ;; Disable documentation in the echo area
  ;; (corfu-scroll-margin 5)        ;; Use scroll margin

  ;; Enable Corfu only for certain modes.
  ;; :hook ((prog-mode . corfu-mode)
  ;;        (shell-mode . corfu-mode)
  ;;        (eshell-mode . corfu-mode))

  ;; Recommended: Enable Corfu globally.
  ;; This is recommended since Dabbrev can be used globally (M-/).
  ;; See also `corfu-excluded-modes'.
  :init
  (global-corfu-mode))

;; aug 25th, 2023: disabled due to error with vertico (void-variable string-width)
(use-package marginalia
  ;; Bind `marginalia-cycle' locally in the minibuffer.  To make the binding
  ;; available in the *Completions* buffer, add it to the
  ;; `completion-list-mode-map'.
  :bind (:map minibuffer-local-map
              ("M-A" . marginalia-cycle))

  ;; The :init configuration is always executed (Not lazy!)
  :init

  ;; Must be in the :init section of use-package such that the mode gets
  ;; enabled right away. Note that this forces loading the package.
  (marginalia-mode)
  )
