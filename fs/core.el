;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Basic Look&Feel
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

;; Menu and tool bars
(if (and (fboundp 'menu-bar-mode) (not (memq window-system '(mac ns x)))) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))

;; Transparencies
(set-frame-parameter (selected-frame) 'alpha '(100 . 95))
(add-to-list 'default-frame-alist '(alpha . (100 . 95)))

;; Cursor
;(set-cursor-color "#CCCCC7")
;(setq curchg-default-cursor-color "#CCCCC7")
(require 'cursor-chg)  ; Load the library
(toggle-cursor-type-when-idle 1) ; Turn on cursor change when Emacs is idle
(change-cursor-mode 1) ; Turn on change for overwrite, read-only, and input mode
(setq
 cursor-in-non-selected-windows nil ; Hide the cursor in inactive windows.
 )

;; Frame
(use-package frame
  :ensure nil
  :defer
  :init
  ;; Mispressing C-z invokes `suspend-frame' (disable).
  (global-unset-key (kbd "C-z"))
  :config
  ;; Enable expanding frame to end of screen.
  (setq frame-resize-pixelwise t)
  ;; Remove thin border. Visible since Monterey.
  (set-frame-parameter nil 'internal-border-width 0)
  ;; (set-frame-position (selected-frame) 15 53)
  ;; (set-frame-size (selected-frame)
  ;;                 (- (display-pixel-width) 30 16)
  ;;                 (- (display-pixel-height) 30 53 15)
  ;;                 t)
  )

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Generic variables
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(setq
 window-resize-pixelwise nil
 split-width-threshold 80           ;; How thin the window should be to stop splitting vertically
 scroll-margin 1                    ;; smooth scrolling please
 scroll-step 1                      ;; scroll 1 line at a time with the keyboard
 scroll-conservatively 101          ;; move minimum when cursor exits view, instead of recentering
 scroll-preserve-screen-position 1
 mouse-wheel-follow-mouse t         ;; scoll pointed window
 mouse-wheel-scroll-amount '(1 ((shift) . 3) ((meta) . 6))     ;; mouse scroll moves 1 line at a time, instead of 5 lines
 mouse-wheel-progressive-speed nil  ;; on a long mouse scroll keep scrolling by the same amount
 mouse-wheel-tilt-scroll t          ;; enable horizontal scrolling with mouse
 mouse-yank-at-point t
 select-enable-clipboard t
 select-enable-primary nil            ;; keep this nil to prevent automatically copying into clipboard when selecting
 ;; select-active-regions nil
 save-interprogram-paste-before-kill t
 apropos-do-all t
 require-final-newline t
 visible-bell nil                   ;; make it ring
 ring-bell-function 'ignore         ;; but w/o any sound :D
 load-prefer-newer t
 inhibit-startup-screen t
 switch-to-buffer-obey-display-actions t ;; Emacs treats manual buffer switching the same as programmatic switching
 ediff-window-setup-function 'ediff-setup-windows-plain
 tramp-default-method "ssh"         ;; https://www.emacswiki.org/emacs/TrampMode
 vc-follow-symlinks t               ;; Don't warn for following symlinked files
 large-file-warning-threshold nil   ;; Don't warn for large files (shows up when launching videos)
 line-move-visual t
 echo-keystrokes 0.1                ;; Show keystrokes earlier (ie. C-x)
 history-delete-duplicates t        ;; No need to keep duplicates in prompt history
 ;; next are documented here
 ;; https://www.masteringemacs.org/article/demystifying-emacs-window-manager
 switch-to-buffer-in-dedicated-window 'pop
 switch-to-buffer-obey-display-actions t
 )

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Indentation and autocompletion
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; https://www.emacswiki.org/emacs/IndentingText
;; http://ergoemacs.org/emacs/emacs_tabs_space_indentation_setup.html
;; https://www.emacswiki.org/emacs/IndentationBasics
(setq-default
 tab-width 4
 indent-tabs-mode nil               ; always inserts spaces
 tab-always-indent 'complete        ; make tab key do indent first then completion
 fill-column 100
 truncate-lines t)

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Minibuffer and completion settings
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(setq
 ;; Do not allow the cursor in the minibuffer prompt
 ;minibuffer-prompt-properties '(read-only t cursor-intangible t face minibuffer-prompt)
 ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
 ;; Vertico commands are hidden in normal buffers.
 read-extended-command-predicate #'command-completion-default-include-p
 ;; Enable recursive minibuffers
 enable-recursive-minibuffers t
 ;; TAB cycle if there are only few candidates
 completion-cycle-thr 3
 )

;(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Completing indicator
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Add prompt indicator to `completing-read-multiple'.
;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
;; Got this from `vertico' configuration
(defun crm-indicator (args)
  (cons (format "[CRM%s] %s"
                (replace-regexp-in-string
                 "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                 crm-separator)
                (car args))
        (cdr args)))

(advice-add #'completing-read-multiple :filter-args #'crm-indicator)

;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Misc things
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

;; get smooth scrolling like you do in other programs
;; yet, not working very well (4th aug 20223)
;; (pixel-scroll-precision-mode 1)

;; use C-c <left> to undo window/frame changes
(winner-mode 1)

;; Remove trailing whitespace on save
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; find file at point with C-x C-f
(ffap-bindings)

;; just type y or n
(fset 'yes-or-no-p 'y-or-n-p)

;; reload file from FS when it changes
(global-auto-revert-mode 1)

;; use subword mode everywhere
(global-subword-mode 1)

;; UTF-8
;; http://www.wisdomandwonder.com/wordpress/wp-content/uploads/2014/03/C3F.html
(prefer-coding-system 'utf-8)
(when (display-graphic-p)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))

;; make sure region behaves like selection (typing overwrites)
(delete-selection-mode 1)

;; highlight the selected text
(transient-mark-mode 1)

;; highlight current line
;; (global-hl-line-mode)
(dolist (modes '(text-mode-hook
                prog-mode-hook
                conf-mode-hook))
  (add-hook modes (λ (hl-line-mode))))

;; moving among windows using M-s-<arrow>
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings '(meta super)))


;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Line numbering
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

;; column numbering
(column-number-mode 1)

;; Enable line numbers for some modes
;; compare this to: (global-display-line-numbers-mode 1)
(dolist (modes '(text-mode-hook
                prog-mode-hook
                conf-mode-hook))
  (add-hook modes (λ (display-line-numbers-mode 1))))

;; Override some modes which derive from the above
(dolist (modes '(org-mode-hook))
  (add-hook modes (λ (display-line-numbers-mode 0))))
