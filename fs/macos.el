;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; MacOS
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(when (memq window-system '(mac ns))
  (setq
   ns-use-proxy-icon nil                ;; no icon in title bar
   mouse-wheel-flip-direction t         ;; correct left-right scroll direction for OS X
   ns-right-alternate-modifier nil      ;; use the right-alt as modifier to insert special chars on MacOS
   ns-pop-up-frames nil                 ;; When opening a file (like double click) on Mac, use an existing frame
   mac-redisplay-dont-reset-vscroll t
   mac-mouse-wheel-smooth-scroll nil
   ns-use-srgb-colorspace nil           ;; Fixes mode line separator issues on macOS.
   use-dialog-box nil                   ;; Disable dialog boxes since they weren't working in Mac OSX
   trash-directory "~/.Trash"
   )
  (exec-path-from-shell-initialize)         ;; make sure PATH is read from shell

  ;; Transparent titlebar on macOS.
  (add-to-list 'default-frame-alist '(ns-appearance . dark))
  (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))

  ;; See `trash-directory' as it requires defining `system-move-file-to-trash'.
  ;; Remember to install it using `brew install trash'
  (defun system-move-file-to-trash (file)
    "Use \"trash\" to move FILE to the system trash."
    (cl-assert (executable-find "trash") nil "'trash' must be installed. Needs \"brew install trash\"")
    (call-process "trash" nil 0 nil "-F"  file))

  )
