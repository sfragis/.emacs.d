;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Git
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

;; Magit
(use-package magit
  :init
  ;; makes magit full screen
  (defadvice magit-status (around magit-fullscreen activate)
    (window-configuration-to-register :magit-fullscreen)
    ad-do-it
    (delete-other-windows))

  ;; wraps the magit session to store the current buffer and restore it afterwards
  (defun magit-quit-session ()
    "Restores the previous window configuration and kills the magit buffer"
    (interactive)
    (kill-buffer)
    (jump-to-register :magit-fullscreen))

  :bind
  ("<f4>" . 'magit-status)
  ("C-x C-g" . 'magit-status)
  (:map magit-status-mode-map
        ("q" . magit-quit-session))
  )

;; Git Gutter
(use-package git-gutter
  :defer 1
  :hook
  ((prog-mode . git-gutter-mode))
  :config
  (setq
   git-gutter:disabled-modes '(org-mode asm-mode image-mode)
   git-gutter:update-interval 2
   git-gutter:modified-sign "~"
   git-gutter:added-sign "+"
   git-gutter:deleted-sign "-"
   )
  (set-face-foreground 'git-gutter:modified "white")
  (set-face-foreground 'git-gutter:added "green")
  (set-face-foreground 'git-gutter:deleted "magenta")
  ;; sets fringe width
  (if (fboundp 'fringe-mode) (fringe-mode '(4 . 0)))
  ;; places the git gutter outside the margins.
  (setq-default fringes-outside-margins t)
  :bind
  ("C-," . git-gutter:previous-hunk)
  ("C-." . git-gutter:next-hunk)
  ("C-±" . git-gutter:popup-hunk)
  )
