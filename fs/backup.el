;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
;; Backup
;; ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
(defvar fs/backup-directory (concat user-emacs-directory "backups"))
(if (not (file-exists-p fs/backup-directory))
        (make-directory fs/backup-directory t))
(setq
 backup-directory-alist `(("." . ,fs/backup-directory))
 make-backup-files t               ; backup of a file the first time it is saved.
 vc-make-backup-files t            ; make backups of files, even when they're in version control
 backup-by-copying t               ; don't clobber symlinks
 version-control t                 ; version numbers for backup files
 delete-old-versions t             ; delete excess backup files silently
 delete-by-moving-to-trash t
 delete-auto-save-files t          ; deletes buffer's auto save file when it is saved or killed with no changes in it.
 kept-old-versions 6               ; oldest versions to keep when a new numbered backup is made (default: 2)
 kept-new-versions 9               ; newest versions to keep when a new numbered backup is made (default: 2)
 auto-save-default t               ; auto-save every buffer that visits a file
 auto-save-timeout 20              ; number of seconds idle time before auto-save (default: 30)
 auto-save-interval 200            ; number of keystrokes between auto-saves (default: 300)
 )

;; Automatically purge backup files not accessed in a week:
(message "Deleting old backup files...")
(let ((week (* 60 60 24 7))
      (current (float-time (current-time))))
  (dolist (file (directory-files (cdr (car backup-directory-alist)) t))
    (when (and (backup-file-name-p file)
               (> (- current (float-time (nth 4 (file-attributes file))))
                  week))
      (message "> %s" file)
      (delete-file file))))

;; Save point position between sessions
(setq save-place-file (expand-file-name ".places" user-emacs-directory))
(save-place-mode 1)
